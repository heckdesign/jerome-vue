// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import LazyLoadDirective from './directives/LazyLoadDirective.js'
import LazyLoad from './directives/LazyLoad.js'

Vue.directive('lazyload', LazyLoadDirective)
Vue.directive('lazyload2', LazyLoad)

Vue.config.productionTip = false

/* eslint-disable no-new */
var app = new Vue({
  el: '#app',
  data: {
    josh: 'Heck'
  },
  router,
  components: { App },
  template: '<App/>',

  watch: {
    $route (to, from) {
      // console.log(to)
      switch (to.name) {
        case 'home':
          this.transitionToUse = 'fade'
          var angle = this.$refs.someAngle
          // console.log(angle);
          break
        case 'about':
          this.transitionToUse = 'slide'
          // console.log(this.children('.angle-right'))
          angle = this.$refs.someAngle
          // console.log(angle);
          break
        default:
          this.transitionToUse = 'fade'
          break
      }
    }
  }, // End watch
  methods: {
    beforeLeave: function (el) {
      console.log('BeforeLeave')
    }
  }
  // mounted() {
  //   console.info('App this router:', this.$router)
  //   console.info('App currentRoute:', this.$router.currentRoute)
  // },
  // computed: {
  //   currentRouteName() {
  //       return this.$route.name;
  //   }
  // }
})

// router.beforeRouteLeave((to, from, next) => {
// Indicate to the SubComponent that we are leaving the route
// this.$refs.mySubComponent.prepareToExit()
// Make sure to always call the next function, otherwise the hook will never be resolved
// Ref: https://router.vuejs.org/en/advanced/navigation-guards.html
// next();
// })
