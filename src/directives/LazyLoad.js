export default{
// const observer = new IntersectionObserver(
//   (entries, observer) => {
//     entries.forEach(entry => {
//     /* Here's where we deal with every intersection */

//       console.log('~~~~~~~~~~~~~~Intersection~~~~~~~~~~~~~~~~')
//       entry.target.classList.add('show')
//     })
//   },
//   { root: null, threshold: 0 })
// document.querySelectorAll('.cube').forEach(cube => { observer.observe(cube) })

  inserted: el => {
    // function loadImage () {
    //   const imageElement = Array.from(el.children).find(
    //     el => el.nodeName === 'IMG'
    //   )
    //   if (imageElement) {
    //     console.log(el)
    //     imageElement.addEventListener('load', () => {
    //       setTimeout(() => el.classList.add('loaded'), 100)
    //       console.log(el.isActive)
    //       // setTimeout(() => el.classList.add('loaded'), 100)
    //     })
    //     imageElement.addEventListener('error', () => console.log('error'))
    //     imageElement.src = imageElement.dataset.url
    //   }
    // }

    function handleIntersect (entries, observer) {
      entries.forEach(entry => {
        // console.log('Entry: ', entry)
        if (entry.isIntersecting) {
          // setTimeout(function () {
          // console.log('Intersecting')
          // console.log(el)
          el.classList.add('show')
          //   loadImage()
          observer.unobserve(el)
          // }, 2000)
        }
      })
    }

    function createObserver () {
      console.log('Create observer')
      const options = {
        root: null,
        threshold: '0',
        rootMargin: '0px 0px -150px 0px'
      }
      const observer = new IntersectionObserver(handleIntersect, options)
      observer.observe(el)
    }
    if (window.IntersectionObserver) {
      createObserver()
    } else {
      loadImage()
    }
  }

}
