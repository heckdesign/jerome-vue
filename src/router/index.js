import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import about from '@/components/about'
import contact from '@/components/contact'
import portfolio from '@/components/portfolio'
import featured from '@/components/featured'
import portfolio2 from '@/components/portfolio2'
import VueRouter from 'vue-router'
// import VueColcade from 'vue-colcade'
// Vue.use(VueColcade)
Vue.use(Router)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      // name: 'home',
      component: home
    },
    {
      path: '/about',
      // name: 'about',
      component: about
    },
    {
      path: '/featured',
      // name: 'featured',
      component: featured
    },
    {
      path: '/portfolio',
      name: 'portfolio',
      component: portfolio
    },
    {
      path: '/portfolio2',
      name: 'portfolio2',
      component: portfolio2
    },
    {
      path: '/contact',
      name: 'contact',
      component: contact
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    // return desired position
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  el: '#angle-right',
  data: {
    show: true
  }
  // methods: {
  //   beforeRouteLeave (to, from, next) {
  //     console.log('B4 ROUTE LEAVE')
  //     next()
  //   }
  // }
})

// router.beforeRouteLeave((to, from, next) =>{
// const answer = window.confirm('Do you really want to leave? you have unsaved changes!')
// if (answer) {
//   next()
// } else {
//   next(false)
// }
// })

// router.beforeRouteLeave((to, from, next) => {
//   // Indicate to the SubComponent that we are leaving the route
//   this.$refs.mySubComponent.prepareToExit();
//   // Make sure to always call the next function, otherwise the hook will never be resolved
//   // Ref: https://router.vuejs.org/en/advanced/navigation-guards.html
//   next()
// })

router.beforeEach((to, from, next) => {
  // console.log('BEFORE EACH')
  // if (to.matched.some(record => record.meta.shouldBustCache)) {
  //   bustCache()
  // }
  next()
})

export default router
