'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@main.sass";`
      }
    }
  }
})
